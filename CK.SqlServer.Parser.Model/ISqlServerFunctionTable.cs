using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.SqlServer.Parser;

/// <summary>
/// Defines multi-statements function table.
/// </summary>
public interface ISqlServerFunctionTable : ISqlServerCallableObject
{
}
