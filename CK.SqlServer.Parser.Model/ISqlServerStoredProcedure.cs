using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.SqlServer.Parser;

/// <summary>
/// Defines a stored procedure.
/// </summary>
public interface ISqlServerStoredProcedure : ISqlServerCallableObject
{
}
