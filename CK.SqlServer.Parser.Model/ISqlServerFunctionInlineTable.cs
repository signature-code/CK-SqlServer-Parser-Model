using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.SqlServer.Parser;

/// <summary>
/// Defines an inline function table.
/// </summary>
public interface ISqlServerFunctionInlineTable : ISqlServerCallableObject
{
}
